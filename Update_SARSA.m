function [ Q ] = Update_SARSA( s, a, r, sp, ap, Current_Q, alpha, gamma )

Q = Current_Q;
Q(s,a) = Q(s,a) + alpha * (r + gamma*Q(sp,ap) - Q(s,a));

end

