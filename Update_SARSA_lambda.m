function [ Q, E ] = Update_SARSA_lambda( s, a, r, sp, ap, Q, E, alpha, gamma, lambda)

E = (gamma*lambda).*E;                      %accumulating traces
    
E(s,a) = 1;                                 %replacing traces

tempdiff = r + (gamma*Q(sp,ap))- Q(s,a);    %temporal difference - TD

Q = Q + (alpha * tempdiff) .* E;            %Update Q table

end
