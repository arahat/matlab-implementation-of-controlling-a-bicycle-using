%==========================================================================
%Project:                       Learning control using Reinforcement 
%                               Learning (SARSA(lambda)) - Bicycle
%
%Written by:                    Alma A. M. Rahat                               
%                               Project Support Assistant,
%                               Information: Signals, Images, Systems 
%                               (ISIS) Research Group,
%                               School of Electronics and Computer Sciences
%                               University of Southampton
%                               [pt. III,BEng in Electronic Engineering]
%                               almarahat@gmail.com
%                               http://www.almarahat.site11.com
%
%In collaboration With:         Prof. Mahesan Niranjan
%                               Head of Group
%                               Information: Signals, Images, Systems 
%                               (ISIS) Research Group,
%                               School of Electronics and Computer Sciences
%                               University of Southampton
%                               http://www.ecs.soton.ac.uk/people/mn
%
%Modifed from:                  Cart-Pole Demo with SARSA programmed in
%                               MATLAB
%                               by:
%                               Jose A. Martin H.
%                               jmartinh@fdi.ucm.es
%                               http://www.dia.fi.upm.es/~jamartin/
%==========================================================================

function [] =  BDemo(maxIter, maxEpi )

%maxIter: no. of agents for acquiring the average
%maxEpi: no. of trials to conduct


%--------------------------------------------------------------------------
%Initialization:
%--------------------------------------------------------------------------

tic
disp('======== Bicycle Control using SARSA(lambda) ========');

x = zeros(maxIter, maxEpi);                         %memory allocation for 
                                                    %x-axis data
y = zeros(maxIter, maxEpi);                         %memory allocation for 
                                                    %y-axis data

statelist = Build_State_List();                     %Build state list for 
[index, actions] =  Build_Action_List();            %acquiring possible 
                                                    %number of states and 
nstates = size(statelist, 1);                       %actions
nactions = size(index, 1);

clear statelist;                                    %clear unnecessary data
clear index;
clear actions;

Qbuf = zeros(nstates,nactions,maxIter);             %buffer for Q
                                                    %memory (3D matrix)
sVbuf = zeros(nstates, 1, maxIter);                 %buffer for states 
                                                    %visited (3D matrix)
aVbuf = zeros(nstates, nactions, maxIter);          %buffer for acitons 
                                                    %visited (3D matrix)

Q = zeros (nstates, nactions);
sV = zeros(nstates,1);
aV = zeros(nstates, nactions);


%--------------------------------------------------------------------------
%parallel for loop to enhance time performance of code:
%--------------------------------------------------------------------------

%to use 'parfor' initiate the process using 'matlabpool' in matlab CLI
%parallel computing toolbox is necessary in this case. In case of
%unavailability use normal for loop!

parfor i = 1:maxIter                                
    
    disp(['Iteration Started: ', int2str(i)]);
    
    [x(i,:), y(i,:), Qbuf(:,:,i), sVbuf(:,:,i), aVbuf(:,:,i)] = Bicycle_demo(maxEpi, i);
    
    disp(['Iteration completed: ', int2str(i)]);
    disp(['===================================']);
    
end

%--------------------------------------------------------------------------
%gather random single agent result to compare with average result:
%--------------------------------------------------------------------------

%first agent has been used to generate these

xRand = x(1,:);                                     
yRand = y(1,:);
QRand = Qbuf(:,:,1);
sRand = sVbuf(:,:,1);
aRand = aVbuf(:,:,1);


%--------------------------------------------------------------------------
%Average 'maxIter' agent result:
%--------------------------------------------------------------------------

x = sum(x)/maxIter;
y = sum(y)/maxIter;

for j=1:maxIter
    Q = Q + Qbuf(:,:,j);
    sV = sV + sVbuf(:,:,j);
    aV = aV + aVbuf(:,:,j);
end

Q = Q / maxIter;
sV = sV /maxIter;
aV = aV / maxIter;


%--------------------------------------------------------------------------
%save results:
%--------------------------------------------------------------------------

save x.txt x -ASCII;
save y.txt y -ASCII;
save xRand.txt xRand -ASCII;
save yRand.txt yRand -ASCII;
save Q.txt Q -ASCII;
save QRand.txt QRand -ASCII;
save states_visited_avg.txt sV  -ASCII;
save states_visited_rand.txt sRand -ASCII;
save actions_visited_avg.txt aV  -ASCII;
save actions_visited_rand.txt aRand -ASCII;


%--------------------------------------------------------------------------
%plot steps vs. episodes:
%--------------------------------------------------------------------------

plot(x,y)                                           
title(['Average after iteration:  ',int2str(maxIter)]) 
xlabel('Episodes')
ylabel('Steps')  
drawnow

toc
end

