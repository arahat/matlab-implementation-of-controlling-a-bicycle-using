
function [ a ] = e_greedy( Q, state, epsilon )

actions = size(Q,2);                %how many possible actions

if(rand()>epsilon)
    a = Best_Action(Q, state);      %Greedy action    
else
    a = randint(1,1,actions)+1;     %a = randi(actions,1);
                                    %replace by this for MATLAB 2010
                                    %random action selection
end

end

