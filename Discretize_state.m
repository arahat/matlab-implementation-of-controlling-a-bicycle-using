function [ s, x ] = Discretize_state( x, statelist )

x = repmat(x, size(statelist,1), 1);

[v, s] = min(sqrt(sum((statelist-x).^2,2)));    %the closest state with the 
                                                %current state
                                                %v = value
                                                %s = index
x = [ x(s,1) x(s,2) x(s,3) x(s,4) x(s,5)];      %retain percieved state

end


