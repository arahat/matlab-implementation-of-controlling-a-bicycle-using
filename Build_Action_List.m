

function [ ActionIndex, Actions ] = Build_Action_List( )

%9 actions, replace by teh commented parts for 25 actions
T = [-2 0 2];               %-2:1:2;        %torque on the han handle bars
d = [-0.02 0 0.02 ];        %-0.02:0.01:0.02;%CoM displacement
index=1;

Actions = zeros(9,2);       %zeros(25,2);   % memory allocation for actions

for i = 1:3                 %1:5
    for j=1:3               %1:5
        Actions(index,1) = T(i);
        Actions(index,2) = d(j);
        index=index+1;
    end
end

actions = 1:1:9;            %1:1:25;

ActionIndex = actions';     %actions index to point out each set of 
                            %acitons (row)

end

