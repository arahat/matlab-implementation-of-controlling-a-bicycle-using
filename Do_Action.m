function [ phi, state, xf, yf ] = Do_Action( T, d, state, xf, yf)

w = state(1);
w_dot = state(2);
w_dot_dot = state(3);               %This could be excluded from the model
                                    %necessary modifications have to be
                                    %considered
theta = state(4);
theta_dot = state(5);

thetap = theta;

s = 0.02;                           
p =2*rand()-1;

d = d + s*p;                        %noise addition

persistent c  ; c = 0.66;           %horizontal distance between the point,
                                    %where the front wheel touches the 
                                    %ground and the Centre of Mass 
persistent dCM; dCM = 0.3;          %The vertical distance between the CM 
                                    %for the bicycle and for the cyclist
persistent h; h   = 0.94;           %height of CM over the ground
persistent l; l   = 1.11;           %Distance between the front tyre and the back tyre at the point where they touch the ground
persistent Mc; Mc  = 15;            %the mass of the bicycle
persistent Md; Md  = 1.7;           %The mass of a tyre
persistent Mp; Mp  = 60;            %The mass of the cyclist
persistent r; r   = 0.34;           %radius of a tyre
persistent v; v   = 2.778;          %velocity of the cycle
persistent g; g   = 9.8;            %gravitational acceleration

persistent dt; dt  = 0.01;          %time difference
persistent M ;   
M = Mc + Mp + (2*Md);               %Total mass
persistent sigma_dot; 
sigma_dot   = v/r;                  %the angular velocity of the bicycle
phi         = w + atan(d/h);        %total angle of tilt

persistent Icp; 
Icp = (13/3)*Mc*h*h + Mp*(h+dCM);   %moment of inertia for the cycle and cyclist
persistent Idc; 
Idc = Md*r*r;                       %moment of inertia for the tyres
persistent Idv; 
Idv = (3/2)*Md*r*r;  
persistent Idl; 
Idl = (1/2)*Md*r*r;  

rf          = l/abs(sin(theta));    %front tyre from the centre of 
                                    %centrifugal force

rb           = l/abs(tan(theta));   %back tyre

rCM         = sqrt((l-c)*(l-c) + (l*l/(tan(theta)*tan(theta))));    
                                    %Centre of mass

w_dot_dot   = (1/Icp)*((M*g*h*sin(phi)) - cos(phi)*((Idc*sigma_dot*theta_dot)+(sign(theta)*(v*v*((Md*r/rf)+(Md*r/rb)+(M*h/rCM))))));
theta_dot_dot = ((T - (Idv*sigma_dot*w_dot))/Idl);

w_dot = w_dot + (dt*w_dot_dot);
w = w + (dt*w_dot);

theta_dot = theta_dot+(dt*theta_dot_dot);
theta = (theta+dt*theta_dot);


psi = theta - thetap;

xf = xf - v*dt*(sin(psi+sign(psi)*asin(v*dt/2/rf)));
yf = yf + v*dt*(cos(psi+sign(psi)*asin(v*dt/2/rf)));

state = [w w_dot w_dot_dot theta theta_dot];

end


