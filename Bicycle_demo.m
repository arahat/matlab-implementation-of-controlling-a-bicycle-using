function [xP, yP, Q, statesVisited, actionsVisited ]= Bicycle_demo( maxepisodes, iter )

%--------------------------------------------------------------------------
%Initialization:
%--------------------------------------------------------------------------

maxsteps = 1000;                                        %maximum steps in 
                                                        %each episode
statelist = Build_State_List();                         %discretized 
                                                        %statelist creation
[ActionIndex Actions] = Build_Action_List();            %discretized action
                                                        %list creation

nstates  = size(statelist, 1);                          %no. of possible 
                                                        %states in 
                                                        %particular app
nactions = size(ActionIndex, 1);                        %no. of possible 
                                                        %actions in 
                                                        %particular app
Q        = Build_Q_Table(nstates,nactions);             %Build the 
                                                        %state-action 
                                                        %memory matrix 
                                                        %(Q -Table)
E        = Build_Q_Table(nstates, nactions);            %Eligibility Matrix

alpha    = 0.5;                                         %learning rate
gamma    = 0.99;                                        %discount factor
epsilon  = 0.01;                                        %probability of 
                                                        %choosing the 
                                                        %greedy policy
                                                        %/action
                                                        %(exploration / 
                                                        %exploitation 
                                                        %balance could be 
                                                        %managed by this 
                                                        %value)
lambda   = 0.95;                                        %decay rate

statesVisited  = zeros(nstates,1);
actionsVisited = Build_Q_Table(nstates, nactions);

%--------------------------------------------------------------------------
%3D animation: (uncomment the following)
%--------------------------------------------------------------------------

world = vrworld('Bicycle');                             %3D world init
open(world);                               
fig = vrfigure(world);                                  %figure object
handle = vrnode (world, 'front');                       %handle object,it's
                                                        %called 'front' the
                                                        %model
bike = vrnode(world, 'Bicycle');                        %similar to handle
FWheel = vrnode(world, 'FWheel');
RWheel = vrnode(world,'RWheel');


%[x, y] =  meshgrid(1:1:nactions, 1:1:nstates);         %for 3D 
                                                        %visualization of 
                                                        %the Q table
                                                        %use surf(x,y,Q) 
                                                        %to draw
                                                        %the Q table 

%--------------------------------------------------------------------------
%Episodes:
%--------------------------------------------------------------------------

%parallel computing would not work here, as previous experience is vital !

for i=1:maxepisodes
    
    [total_reward,steps,Q, statesVisited, actionsVisited] = Episode(maxsteps, Q, E, alpha, gamma, epsilon, statelist, Actions, lambda, statesVisited, actionsVisited, fig, handle, FWheel, RWheel, bike); 
                                                        
    epsilon = epsilon * 0.99;                           %Decaying the prob.
                                                        %of exploration and 
                                                        %giving priority to
                                                        %best action 
                                                        %selection with the 
                                                        %increase in time        
    xpoints(i)=i-1;
    xP = xpoints;
    ypoints(i)=steps;     
    yP = ypoints;
    
    %disp(total_reward);                                %To display total
                                                        %reward
    
    if(mod(i,500)== 0)
    
    disp(['Iteration no.: ', num2str(iter),'Episodes gone: ', int2str(i)]); 
                                                        %could be ignored,
                                                        %solely because
                                                        %looking at the
                                                        %monitor without
                                                        %anything
                                                        %happenning is
                                                        %quite boring!
    end
    
end

end

