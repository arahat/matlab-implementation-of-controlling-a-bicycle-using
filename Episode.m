function [ Total_Reward, steps, Q, sV, aV] = Episode( maxsteps, Q, E, alpha, gamma, epsilon, statelist, Actions, lambda , sV, aV,  fig, handle, FWheel, RWheel, bike)

x        = [0 0 0 0 0];                         %intial state
steps        = 0;                               %no. of steps init
Total_Reward = 0;                               %Total reward init

s = Discretize_state (x, statelist);            %Get the state index
sV(s) = sV(s) + 1;                              %states visited increase 
                                                %for respective state
                                                %no. of times

a = e_greedy(Q,s,epsilon);                      %suitable action selection
aV(s,a) = aV(s,a) + 1;                          %action taken increase
                                                %for respective action
                                                %no.of times

xf = [];                                        %front tyre x-axis location
yf = [];                                        %front tyre y-axis location
xff=0;                                          
yff=0;

E = 0.*E;                                       %set eligibility trace to 
                                                %zero

ang = linspace(-pi, pi, 10);                    %angle span for wheel 
                                                %rotation

for i=1:maxsteps                                %run the loop till it falls
    
    T = Actions(a,1);
    d = Actions(a,2);
    
    FWheel.rotation = [0 0 1 ang(1)];           %rotate wheels
    RWheel.rotation = [0 0 1 ang(1)];           %FWheel = front
    vrdrawnow;                                  %RWheel = rear
    
    xf(i) = xff;
    yf(i) = yff;
    
    FWheel.rotation = [0 0 1 ang(2)];
    RWheel.rotation = [0 0 1 ang(2)];
    vrdrawnow;
    
    [phi, xp, xff,  yff] = Do_Action(T, d, x, xff, yff);
                                                %implement action and
                                                %observe next state
    
    FWheel.rotation = [0 0 1 ang(3)];
    RWheel.rotation = [0 0 1 ang(3)];
    vrdrawnow;
    
    [ r, goal ] = Get_Reward(phi, xp(4));       %Get reward for taken action
    Total_Reward = Total_Reward + r;
    
    FWheel.rotation = [0 0 1 ang(4)];
    RWheel.rotation = [0 0 1 ang(4)];
    vrdrawnow;
        
    [sp, xp] = Discretize_state(xp, statelist); %index of next state
    
    FWheel.rotation = [0 0 1 ang(5)];
    RWheel.rotation = [0 0 1 ang(5)];
    vrdrawnow;
    
    ap = e_greedy(Q,sp,epsilon);                %action for next state
    
    FWheel.rotation = [0 0 1 ang(6)];
    RWheel.rotation = [0 0 1 ang(6)];
    vrdrawnow;
    
    %Update Q table - uncomment second one and comment out first for SARSA
    [Q, E] = Update_SARSA_lambda (s, a, r, sp, ap, Q, E, alpha, gamma, lambda);
    %Q = Update_SARSA( s, a, r, sp, ap, Q, alpha, gamma );
    
    FWheel.rotation = [0 0 1 ang(7)];
    RWheel.rotation = [0 0 1 ang(7)];
    vrdrawnow;
    
    s = sp;                                     %current state index = next
                                                %state
    a = ap;                                     %Current action index = 
                                                %next action index
    x = xp;                                     %current state = next
                                                %state
    
    FWheel.rotation = [0 0 1 ang(8)];
    RWheel.rotation = [0 0 1 ang(8)];
    vrdrawnow;
    
    steps = steps+1;                            %increase no. of steps
    
    handle.rotation = [0 1 0  x(4)];            %roatate handle according 
                                                %to theta
    bike.rotation = [ 1 0 0  phi];              %change centre of mass
                                                %according to phi
    
    FWheel.rotation = [0 0 1 ang(9)];           
    RWheel.rotation = [0 0 1 ang(9)];
    vrdrawnow;
    
    if(goal == true)
        %show the bicycle has fallen
        if(phi>0)
           bike.rotation = [ 1 0 0  deg2rad(90)];
        else
           bike.rotation = [ 1 0 0  deg2rad(-90)];
        end
        vrdrawnow;
        pause(0.5);                             %pause to create interval
                                                %between episodes
        break
    end
    
    sV(s) = sV(s) + 1;
    aV(s,a) = aV(s,a) + 1;
    
    %draw the top view of the path (following the front wheel)
    line(xf,yf)
    title('Top View of Path');
    drawnow

end

end

