Note:
This code has not been tested with recent versions of Matlab, and hence the 3D animation may not work. 

This is an online simulation of learning to ride a bicycle using reinforcement 
learning (SARSA - lambda) algorithm. Here an agent tries different actions at 
different states and receives rewards or punishments based on whether its 
actions leads to successfully balancing the bike or not. After many iterations 
of such learning epochs, the agent learns to balance the bike. Here we 
re-produced parts of what has been presented in the following paper, with a view
to demonstrate the usefulness of reinforcement learning in solving a real world 
problem.

Randløv, Jette, and Preben Alstrøm. "Learning to Drive a Bicycle Using 
Reinforcement Learning and Shaping." ICML. Vol. 98. 1998.

================================================================================
Project:                       Learning control using Reinforcement 
                               Learning (SARSA(lambda)) - Bicycle

Funded By:                     School of Electronics and Computer Sciences
                               University of Southampton

Written by:                    Alma A. M. Rahat                               
                               Project Support Assistant,
                               Information: Signals, Images, Systems 
                               (ISIS) Research Group,
                               School of Electronics and Computer Sciences
                               University of Southampton
                               [pt. III,BEng in Electronic Engineering]
                               almarahat@gmail.com

In collaboration With:         Prof. Mahesan Niranjan
                               Head of Group
                               Information: Signals, Images, Systems 
                               (ISIS) Research Group,
                               School of Electronics and Computer Sciences
                               University of Southampton
                               http://www.ecs.soton.ac.uk/people/mn

Modifed from:                  Cart-Pole Demo with SARSA programmed in
                               MATLAB
                               by:
                               Jose A. Martin H.
                               jmartinh@fdi.ucm.es
                               http://www.dia.fi.upm.es/~jamartin/

================================================================================
Further Notes:

Title. Matlab Implementation of Controlling a Bicycle Using Reinforcement 
Learning
Copyright (C)  2010 Alma A. M. Rahat

This implementation is free: you can redistribute it and/or modify it. It would 
be appreciated if the author is acknowledged accordingly.

This implementation is distributed with the hope that it will be useful; but 
comes without any warranty; without even the implied warranty of merchantability 
or fitness for a particular purpose.

Running the program:

To run the program type 
>> BDemo( no. of agents to take average from , Maximum no. of episodes to run)

Note here, maximum no. of episodes means the highest no. of trials the agent 
should conduct.

As output one should exect to see online animation and path diagram. Once 
the program ends, a steps vs. episodes diagram would appear and a few files 
containing crucial information would be saved. Different graphs could be 
produced using these graphs.

Any problems/bugs should be directed to the writer.

================================================================================

File Description:
_________________

**consult comments for further understanding

BDemo.m:
Starts the program. This is designed to take average for multiple agents 
and save necessary data. 

Best_Action.m:
Finds the greedy action by picking out the action index with highest Q 
value.

Bicycle.WRL:
This is the VRML file that is being used for online animation. This could 
be modified using vrbuild2.exe in matlab installation folder.

Bicycle_demo.m:
This m-file simulate a single agent over maximum no. of trials and makes 
the agent to evolve over time to learn the task using SARSA (lambda).
online 3d animation starts here. should be commented out here and in 
Episode.m for faster simulation.

Build_Action_List.m:
Creates the action list. The default is 9 actions, which could be changed to 
25 by uncommenting and replacing stuff. 

Build_Q_Table.m:
Builds the Q memory according to the no. of states and no. of actions.

Build_State_List.m:
Creates state list. This could be varied according to the system 
specification.

Discretize_state.m:
Used to find the nearest state from the statelist. It subtracts the current
state from all the states in the statelist and then takes the index of the 
nearest one.

Do_Action.m:
Implements the action by calculating the effect of different actions on the 
system. This would change for different systems.

e_greedy.m:
Simulates the epsilon greedy behavior (exploration / exploitation)

Episode.m: 
Runs trials till the maximum no. of trials. Different update scheme could 
be checked. Here the options are SARSA and SARSA (lambda)

Get_Reward.m:
Gets rewarded. Different reward schemes could be used.

Update_SARSA.m:
SARSA update scheme. 

Update_SARSA_lambda.m:
SARSA (lambda) update scheme.

================================================================================