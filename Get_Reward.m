function [ r, goal ] = Get_Reward(phi, theta)

%SHAPING reward scheme for 0 degree target destination, 
%replace by commented one for static
r = 0.00004*(4-theta*theta);            %0.01;

goal = false;

deg12 = deg2rad(12);

if( phi>deg12 || phi<-deg12 ||theta>pi/2 || theta<-pi/2)% | phi>deg12 | phi<-deg12)
    r = -1;                             %punishement for falling down
    goal = true;                        
end

end


